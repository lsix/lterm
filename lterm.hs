module Lterm ( decorateAction
             , decorateGenericAction
             , decorateMany
             , ActionResult(..)
             )
where

import System.Console.Terminfo.Base
import System.Console.Terminfo.Color
import System.Console.Terminfo.Cursor
import System.IO

-- ActionResult describes if an action succedded or failed.
data ActionResult = Success -- ^ Indicates that the action succedded
                  | Failure -- ^ Indicates that the action failed
                  | Unknown -- ^ Indicates unknown state

-- Get message depending of Action result
message :: ActionResult
        -> (String, Color)
message Success = ("OK", Green)
message Failure = ("FAILED", Red)
message Unknown = ("--", Yellow)

-- Show the status
showAtEnd :: ActionResult -- ^ Indicates what should be displayed
          -> IO ()        -- ^ decorated action
showAtEnd s = do
    t <- setupTermFromEnv
    let columns = getCapability t termColumns
        moveToCol = getCapability t columnAddress
        withColor = getCapability t withForegroundColor
        (msg, color) = message s
        moveCmd = case (columns, moveToCol) of
                       (Just c, Just m) -> [m (c - length msg - 3)]
                       (_, _) -> []
        txtCmd = case withColor of
                     Just w -> [w color $ termText msg]
                     _ -> [termText msg]
    mapM_ (runTermOutput t) $ moveCmd ++ [termText "["] ++ txtCmd ++ [termText "]"]
    putStrLn ""

-- Decorate an action. Show the first argument on the screen, then
-- runs the action, and print a status depending of the action.
decorateAction :: String          -- ^ A string describing the action
               -> IO ActionResult -- ^ The action to perform
               -> IO ()           -- ^ A decorated action showing the advancement
decorateAction msg act = putStr' msg >>
                         act >>= \r -> showAtEnd r

decorateGenericAction :: String -- ^ Description of the action
                      -> IO ()  -- ^ The action to perform
                      -> IO ()  -- ^ The decorated actoin
decorateGenericAction msg act = putStr' msg >>
                                act >> showAtEnd Unknown

-- Decorate a set of actions
decorateMany :: [(String, IO ActionResult)] -- ^ list of action and description
             -> [IO ()]                     -- ^ Decorated list of action
decorateMany = map $ uncurry decorateAction

-- Similar to System.IO.putStr but flushed
putStr' :: String -> IO ()
putStr' m = putStr m >> hFlush stdout
